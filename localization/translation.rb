# Extends every ActiveRecord Class with .translation method which takes
# the language code as parameter

module Translation
  def translation(lang)
    self.title = self["title_#{lang.language_code}"]
    self.description = self["description_#{lang.language_code}"]
    return self
  end
end

class Trip
  include Translation
end

class Location
  include Translation
end

class Page
  include Translation
end

class TripCategory
  include Translation
end

class LocationCategory
  include Translation
end

class Collection
  include Translation
end
