module LabelTranslation
  def self.labels(lang_code="en")
    @labels[lang_code.to_sym]
  end

  @labels = {
    :en => {
      :admin_index => {
        :title => "Administration",
        :create_trip => "Create new Trip",
        :create_location => "Create new Location",
        :create_page => "Create new Page",
        :create_trip_category => "Create new Trip Category",
        :create_location_category => "Create new Location Category",
        :create_collection => "Create new Collection",
        :recent_trips => "Recent Trips",
        :recent_locations => "Recent Locations",
        :recent_pages => "Recent Pages",
        :recent_trip_categories => "Recent Trip Categories",
        :recent_location_categories => "Recent Location Categories",
        :recent_collections => "Recent Collections"
      },
      :admin_header => {
        :trips => "Trips",
        :locations => "Locations",
        :pages => "Pages",
        :tripcategories => "Trip Categories",
        :locationcategories => "Location Categories",
        :import => "Import",
        :export => "Export"
      },
      :admin_form => {
        :general => "General",
        :save => "Save",
        :delete => "Delete",
        :edit => "Edit",
        :more => "More",
        :publish => "Publish",
        :unpublish => "Unpublish",
        :published => "Published",
        :unpublished => "Unpublished"
      },
      :admin_location_search => {
        :results => "Results",
        :search_placeholder => "Search Location"
      },
      :admin_trip => {
        :title_overview => "Trips",
        :title_new => "New Trip",
        :title_edit => "Edit Trip",
        :title_view => "Trip",
        :create => "Create new Trip",
        :image => "Image",
        :duration => "Duration (in minutes)",
        :location_category => "Location Category",
        :trip_legend => "Trip",
      },
      :admin_location => {
        :title_overview => "Locations",
        :title_new => "New Location",
        :title_edit => "Edit Location",
        :title_view => "Location",
        :create => "Create new Location",
        :street => "Street",
        :city => "City",
        :postal_code => "Postal Code",
        :country_code => "Country Code",
        :latitude => "Latitude",
        :longitude => "Longitude",
        :duration => "Duration (in minutes)",
        :location_category => "Location Category",
        :image => "Image",
        :search_address => "Search Address"
      },
      :admin_page => {
        :title_overview => "Pages",
        :title_new => "New Page",
        :title_edit => "Edit Page",
        :title_view => "Page",
        :create => "Create new Page",
        :name => "Name"
      },
      :admin_tripcategory => {
        :title_overview => "Trip Categories",
        :title_new => "New Trip Category",
        :title_edit => "Edit Trip Category",
        :title_view => "Trip Category",
        :create => "Create new Trip Category",
        :color => "Color (hex)"
      },
      :admin_locationcategory => {
        :title_overview => "Location Categories",
        :title_new => "New Location Category",
        :title_edit => "Edit Location Category",
        :title_view => "Location Category",
        :create => "Create new Location Category",
        :color => "Color (hex)"
      },
      :admin_translation_form => {
          :title => "Translations",
          :title_label => "Title",
          :description_label => "Description"
      },
      :admin_actionbar => {
        :delete_popup => "Do you really want to delete this object?"
      },
      :client_index => {
        :title_trips => "Trips",
        :title_collections => "Collections",
        :places => "Places",
        :time_unit => "Minutes"
      },
      :client_trip => {
        :switch_back => "Go Back",
        :switch_map => "Map",
        :switch_info => "List",
        :locations_label => "Locations",
        :places => "Places",
        :time_unit => "Minutes",
        :location_more => "More",
        :location_less => "Less"
      },
      :client_contact => {
        :email => "Email",
        :email_placeholder => "jane@doe.com",
        :name => "Name",
        :name_placeholder => "Jane Doe",
        :message => "Message",
        :submit => "Send",
        :success_message => "Thank you for your message!",
        :error_message => "There was an error please try sending your message directly to <a href='mailto:citochris@gmail.com'>citochris@gmail.com</a>!"

      },
      :client_feedback => {
        :closed_button => "Send Feedback",
        :open_button => "Close"
      },
      :client_sitemap => {
        :pages => "Pages",
        :trips => "Trips"
      },
      :client_footer => {
        :attribution => "Made with <span class='heart'>♥</span> by Christian Cito &amp; Nathanael Nußbaumer"
      },
      :client_header => {
        :about => "About",
        :contact => "Contact",
        :imprint => "Imprint",
        :sitemap => "Sitemap"
      }
    },
    :de => {
      :admin_index => {
        :title => "Administration",
        :create_trip => "Neuen Trip erstellen",
        :create_location => "Neuen Ort erstellen",
        :create_page => "Neue Seite erstellen",
        :create_trip_category => "Neue Trip Kategorie",
        :create_location_category => "Neue Ort Kategorie",
        :create_collection => "Neue Kollektion",
        :recent_trips => "Letzte Trips",
        :recent_locations => "Letzte Locations",
        :recent_pages => "Letzte Seiten",
        :recent_trip_categories => "Letzte Trip Kategorien",
        :recent_location_categories => "Letzte Location Kategorien",
        :recent_collections => "Letzte Kollektionen"
      },
      :admin_header => {
        :trips => "Trips",
        :locations => "Orte",
        :pages => "Seiten",
        :tripcategories => "Trip Kategorien",
        :locationcategories => "Ort Kategorien",
        :import => "Import",
        :export => "Export"
      },
      :admin_form => {
        :general => "Allgemein",
        :save => "Speichern",
        :delete => "Löschen",
        :edit => "Bearbeiten",
        :more => "Mehr",
        :publish => "Veröffentlichen",
        :unpublish => "Unveröffentlichen",
        :published => "Veröffentlicht",
        :unpublished => "Unveröffentlicht"
      },
      :admin_location_search => {
        :results => "Resultate",
        :search_placeholder => "Durchsuche Orte"
      },
      :admin_trip => {
        :title_overview => "Trips",
        :title_new => "Neuer Trip",
        :title_edit => "Trip bearbeiten",
        :title_view => "Trip",
        :create => "Neuen Trip erstellen",
        :image => "Bild",
        :duration => "Dauer (in minuten)",
        :location_category => "Ort Kategorie",
        :trip_legend => "Trip",
      },
      :admin_location => {
        :title_overview => "Orte",
        :title_new => "Neuer Ort",
        :title_edit => "Ort bearbeiten",
        :title_view => "Ort",
        :create => "Neuen Ort erstellen",
        :street => "Straße",
        :city => "Stadt",
        :postal_code => "Postleitzahl",
        :country_code => "Land Code",
        :latitude => "Latitude",
        :longitude => "Longitude",
        :duration => "Dauer (in minuten)",
        :location_category => "Ort Kategorie",
        :image => "Bild",
        :search_address => "Adresse suchen"
      },
      :admin_page => {
        :title_overview => "Seiten",
        :title_new => "Neue Seite",
        :title_edit => "Seite bearbeiten",
        :title_view => "Seite",
        :create => "Neue Seite erstellen",
        :name => "Name"
      },
      :admin_tripcategory => {
        :title_overview => "Trip Kategorien",
        :title_new => "Neue Trip Kategorie",
        :title_edit => "Trip Kategorie bearbeiten",
        :title_view => "Trip Kategorie",
        :create => "Neue Trip Kategorie erstellen",
        :color => "Farbe (hex)"
      },
      :admin_locationcategory => {
        :title_overview => "Ort Kategorien",
        :title_new => "Neue Ort Kategorie",
        :title_edit => "Ort Kategorie bearbeiten",
        :title_view => "Ort Kategorie",
        :create => "Neue Ort Kategorie erstellen",
        :color => "Farbe (hex)"
      },
      :admin_translation_form => {
          :title => "Übersetzungen",
          :title_label => "Title",
          :description_label => "Beschreibung"
      },
      :admin_actionbar => {
        :delete_popup => "Willst du wirklich das Objekt löschen?"
      },
      :client_index => {
        :title_trips => "Trips",
        :title_collections => "Kollektionen",
        :places => "Orte",
        :time_unit => "Minuten"
      },
      :client_trip => {
        :switch_back => "Zurück",
        :switch_map => "Karte",
        :switch_info => "Liste",
        :locations_label => "Orte",
        :places => "Orte",
        :time_unit => "Minuten",
        :location_more => "Mehr",
        :location_less => "Weniger"
      },
      :client_contact => {
        :email => "Email",
        :email_placeholder => "max@mustermann.at",
        :name => "Name",
        :name_placeholder => "Max Mustermann",
        :message => "Nachricht",
        :submit => "Senden",
        :success_message => "Danke für deine Nachricht!",
        :error_message => "Es gab einen Fehler. Bitte probiere uns eine Nachricht direkt an <a href='mailto:citochris@gmail.com'>citochris@gmail.com</a> zu schicken!"
      },
      :client_feedback => {
        :closed_button => "Feedback schicken",
        :open_button => "Schließen"
      },
      :client_sitemap => {
        :pages => "Seiten",
        :trips => "Trips"
      },
      :client_footer => {
        :attribution => "Gemacht mit <span class='heart'>♥</span> von Christian Cito &amp; Nathanael Nußbaumer"
      },
      :client_header => {
        :about => "Über uns",
        :contact => "Kontakt",
        :imprint => "Impressum",
        :sitemap => "Sitemap"
      }
    }
  }
end
