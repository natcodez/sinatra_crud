$(document).ready(function () {

  var imageFromMap = function(positions, image) {
    var api_key = "AIzaSyDmCEUHiQfYAmsbCBtqWhXu5SmHwblRJhw"
    var baseurl = "https://maps.googleapis.com/maps/api/staticmap"

    baseurl += "?size=600x400"
    for (var i = 0; i < positions.length; i++) {
      baseurl += "&markers=" + positions[i].position.lat() + "," + positions[i].position.lng()
    }

    baseurl += "&key=" + api_key
    $("[name=img_url]").val(baseurl)
    image.attr("src", baseurl)
  }

  $(".lang-selector .lang").on("click", function(e) {
    lang_code = $(this).attr("data-lang-code")
    location.href = location.href.split("?")[0] + "?lang=" + lang_code
  })

  $(".hex-input input").on("keyup", function(e) {
    $(this).siblings(".hex-symbol").css("background-color", $(this).val())
  })

  $(".color_list li").on("click", function(e) {
    $(".hex-input input")
    .val($(this)
    .attr("data-color")
    .replace("#", ""))
    $(".hex-input input").trigger("keyup")
  })

  var sendFormViaAjax = function(jqEvent) {
    jqEvent.preventDefault()
    form = $(this)
    console.log(form.serialize())
    $.ajax({
        url: form.attr("action"),
        type : form.attr("method"),
        dataType : 'json',
        data : form.serialize()
    }).done(function(data) {
      if(data != null) {
        document.location.href = "/admin"
      }
    })
  }

  var image;
  var imageX = 300;
  var imageY = 200;
  var imageWidth = 300;
  var imageHeight = 200;
  var imageDragable = false;
  var imageScaleable = false;
    //image max scale
  var MAX_WIDTH = 1200;
  var MAX_HEIGHT = 800;
    //3:2 ratio
  var ratioX = 3;
  var ratioY = 2;

  var createTripLocationElHTML = function(trip_location) {
    var html = ""
    html += '<li class="trip_location" data-location-id="' + trip_location.id + '">'
    html += '<div class="trip_location_meta">'
    html += '<span class="trip_location_title">' + trip_location.name + '</span>'
    html += '<button type="button" name="trip_location_expand">More</button>'
    html += '</div>'
    html += '<div class="trip_location_body">'
    html += '<p class="trip_location_description">' + trip_location.desc + '</p>'
    html += '</div>'
    html += '</li>'
    return html;
  }

  $(".trip_location_list").on("click", "button[name=trip_location_expand]", function(e) {
    $(this).closest(".trip_location")
           .find(".trip_location_body")
           .toggleClass("is-visible")
  })

  $(".dropdown_searchbox").on("keyup", function(e) {
    searchString = $(this).val().toLowerCase()
    $(this).siblings(".dropdown_list").children().each(function(ind, el) {
      console.log($(el).text())
      if($(el).text().toLowerCase().indexOf(searchString) === -1) {
        $(el).addClass("is-hidden")
      } else {
        $(el).removeClass("is-hidden")
      }
    })
  })

  $(".trip_locations .dropdown_el").on("click", function(e) {
    $locationEl = $(this)
    location_data = JSON.parse($locationEl.attr("data-location"))
    location_category_data = JSON.parse($locationEl.attr("data-location-category"))
    console.log(location_data)
    $list = $(".trip_location_list")
    if($locationEl.hasClass("is-selected")) {
      $locationEl.removeClass("is-selected")
      $listEl =  $list.find("li[data-location-id=" + location_data.id + "]")
      markers[$listEl.index()].setMap(null)
      markers.splice($listEl.index(), 1)
      $listEl.remove()
    }
    else {
      $locationEl.addClass("is-selected")
      trip_location = {
        id: location_data.id,
        desc: $locationEl.attr("data-location-desc"),
        name: $locationEl.attr("data-location-title"),
        lc: {
          id: location_category_data.id,
          title: $locationEl.attr("data-location-category-title"),
          desc: $locationEl.attr("data-location-category-description"),
          color_hex: location_category_data.color_hex
        }
      }
      $list.append(createTripLocationElHTML(trip_location))

      marker = new google.maps.Marker({
        position: {lat: parseFloat(location_data.latitude), lng: parseFloat(location_data.longitude)},
        map: map
      })

      markers.push(marker)
      console.log(markers)
      imageFromMap(markers, $(".preview"))

      bounds.extend(marker.position)
      map.fitBounds(bounds)
    }
  })

  $(".admin-form-new").on("submit", sendFormViaAjax)

  $(".admin-form-import").on("submit", function(e) {
    e.preventDefault()
    form = $(this)
    form_data = new FormData($(this)[0])

    console.log(form_data)
    $.ajax({
        url: form.attr("action"),
        type : form.attr("method"),
        processData: false,
        contentType: false,
        dataType : 'json',
        data : form_data
    }).done(function(data) {
      if(data.success === 1) {
        alert("Import successful")
      }
    })
  })

  $(".admin-new-location").on("submit", function(e){

    e.preventDefault()
    form = $(this)
    data = form.serializeArray()
    form_data = new FormData($(this)[0])

    console.log(form_data)
    $.ajax({
        url: form.attr("action"),
        type : form.attr("method"),
        processData: false,
        contentType: false,
        dataType : 'json',
        data : form_data
    }).done(function(data) {
      console.log(data)
      if(data != null) {
        document.location.href = "/admin"
      }
    })
  })

  $(".admin-new-trip").on("submit", function(e){
    e.preventDefault()
    form = $(this)
    data = form.serializeArray()
    form_data = new FormData($(this)[0])

    trip_locations = $(".trip_location").map(function(){
      return parseInt($(this).attr("data-location-id"))
    }).get()
    form_data.append("trip_locations", trip_locations)
    console.log(form_data)
    $.ajax({
        url: form.attr("action"),
        type : form.attr("method"),
        processData: false,
        contentType: false,
        dataType : 'json',
        data : form_data
    }).done(function(data) {
      console.log(data)
      if(data != null) {
        document.location.href = "/admin"
      }
    })
  })

  $("[name=delete_button]").on("click", function(e) {
    if(confirm($(this).attr("data-prompt-text"))) {
      action = $(this).attr("data-action")
      $.ajax({
        url: action,
        type: 'delete',
        dataType: 'json'
      }).done(function(data) {
        if(data.success === 1) {
          document.location.href = "/admin"
        } else {
          alert(data.error_msg)
        }
      })
    }
  })

  $("[name=edit_button]").on("click", function(e) {
    action = $(this).attr("data-action")
    document.location.href = action;
  })

  $("[name=toogle_publish_button]").on("click", function(e) {
    action = $(this).attr("data-action")
    $.ajax({
      url: action,
      type: 'post',
      dataType: 'json'
    }).done(function(data) {
      if(data.success === 1) {
        document.location.reload()
      }
    })
  })

  var marker;
  $(".location [name=search_location]").on("change", function (e) {
    street_input = $("input[name=street]")
    city_input = $("input[name=city]")
    postal_code_input = $("input[name=postal_code]")
    country_code_input = $("input[name=country_code]")
    latitude_input = $("input[name=latitude]")
    longitude_input = $("input[name=longitude]")

    address = $(this).val()
    geocoder.geocode( { 'address': address}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
          first_result = results[0]
          address_components = first_result.address_components


          if(marker !== undefined) {
            marker.setMap(null)
          }

          map.setCenter(first_result.geometry.location);

          marker = new google.maps.Marker({
            position: first_result.geometry.location,
            map: map
          })

          console.log(marker)
          imageFromMap([marker], $(".preview"))

          street_input.val(address_components[1].short_name + " " + address_components[0].short_name)
          city_input.val(address_components[2].short_name)
          postal_code_input.val(address_components[6].short_name)
          country_code_input.val(address_components[5].short_name)
          latitude_input.val(first_result.geometry.location.lat)
          longitude_input.val(first_result.geometry.location.lng)

        } else {
          alert("NO RESULTS")
        }
      } else {
        alert("Geocode was not successful for the following reason: " + status)
      }
    })
  })

})
