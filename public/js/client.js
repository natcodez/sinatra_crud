var markers
var geocoder
var bounds
var map
function initMap() {
  markers = []
  geocoder = new google.maps.Geocoder()
  bounds = new google.maps.LatLngBounds();
  map = new google.maps.Map(document.getElementById('map'), {
    center: {
      lat: parseFloat(marker_positions[0].latitude),
      lng: parseFloat(marker_positions[0].longitude)
    }
  })

  for (var i = 0; i < marker_positions.length; i++) {
    marker = new google.maps.Marker({
      label: String(i+1),
      position: {
        lat: parseFloat(marker_positions[i].latitude),
        lng: parseFloat(marker_positions[i].longitude)
      },
      animation: google.maps.Animation.DROP,
      infoWindow: new google.maps.InfoWindow({
        content: document.querySelectorAll(".trip-location")[i].innerHTML,
        maxWidth: 300
      })
    })
    bounds.extend(marker.position)
    markers.push(marker)
  }
  map.fitBounds(bounds)

  for (var i = 0; i < markers.length; i++) {
    setMarkerOnMap(markers[i], 400*(i+1))
  }

  map.addListener("click", function() {
    closeAllInfoWindows(markers)
  })

  attachListenerToMarkers(markers, map)
}

function setMarkerOnMap(marker, timeout) {
  window.setTimeout(function(){
    marker.setMap(map)
  }, timeout);
}

function closeAllInfoWindows(markers) {
  for (var i = 0; i < markers.length; i++) {
    markers[i].infoWindow.close()
  }
}

function attachListenerToMarkers(markers, map) {
  for (var i = 0; i < markers.length; i++) {
    markers[i].addListener('click', function() {
      closeAllInfoWindows(markers)
      map.setCenter(this.position)
      this.infoWindow.open(map, this)
    })
  }
}

$(document).ready(function(){

  // DOM/UI Events

  $(".trip-location, .collection-location").on("click", function(e){
    $(this).toggleClass("is-collapsed")
  })

  $(".site-header-mobile-toggle").on("click", function(e) {
    $("#header-nav").toggleClass("is-visible")
  })

  $(".feedback-box-close").on("click", function(e) {
    var $feedbackBox = $(this).parent()
    $feedbackBox.toggleClass("is-closed")

    if ($feedbackBox.hasClass("is-closed")) {
      $(this).text($(this).attr("data-closed-text"))
    } else {
      $(this).text($(this).attr("data-open-text"))
    }
  })

  $(".mobile-switch span").on("click", function(e) {
    $(this).toggleClass("is-active")
    $(this).siblings("span").toggleClass("is-active")

    $("#map").toggle()
    $("#sidebar").toggle()
  })

  $(".lang-selector .lang").on("click", function(e) {
    lang_code = $(this).attr("data-lang-code")
    location.href = location.href.split("?")[0] + "?lang=" + lang_code
  })

  var sendFormViaAjax = function($form, doneCallback) {
    console.log($form.serialize())
    $.ajax({
        url: $form.attr("action"),
        type : $form.attr("method"),
        dataType : 'json',
        data : $form.serialize()
    }).done(function(data) {
      doneCallback(data)
    })
  }

  $(".site-contact-form").on("submit", function(e) {
    e.preventDefault()
    $form = $(this)

    sendFormViaAjax($form, function(data) {
      console.log(data)
      $form.find(".form-wrap").hide()
      if(data != null && data["success"] == 1) {
         $form.find(".success-message").addClass("is-visible")
      } else {
        $form.find(".error-message").addClass("is-visible")
      }
    })
  })

  // Index Animation

  var time = 100;
  $(".trip-thumb").each(function(ind, el) {
    setTimeout(function(){$(el).fadeIn(200)}, time);
    time += 150;
  })
})
