require File.join(File.dirname(__FILE__), 'app.rb')
require 'rack/protection'
use Rack::Protection, :except => [:remote_token, :session_hijacking]
run TravelingDrunkman
