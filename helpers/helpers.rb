module Helpers
  def slug_from_trip_title(title)
    title.downcase.tr("!?@&=+$,.:{}[]/|", "").gsub(" ", "-")
  end

  def protected!
    return if authorized?
    headers['WWW-Authenticate'] = 'Basic realm="Restricted Area"'
    halt 401, "Not authorized\n"
  end

  def authorized?
    @auth ||=  Rack::Auth::Basic::Request.new(request.env)
    @auth.provided? and @auth.basic? and @auth.credentials and @auth.credentials == ['test', 'test']
  end

  def colors
    [
      "#a3695c",
      "#4152be",
      "#83f609",
      "#e40c9c",
      "#d0d22d"
    ]
  end

  def write_to_log(data)
    File.open('./log.txt', 'w') { |file| file.write(data) }
  end

  def modelToJSON(model)
    json = ""
    json << "\"#{model.name}\": "
    json << model.all.to_json
    json
  end

  def jsonToModel(json_data, model)
    json_data[model.name].each do |json_model|
      json_model = json_model.to_json
      write_to_log(json_model)
      object = model.new
      object.from_json(json_model)
      object.save
    end
  end

  class ::String
	def escape_html_special_characters
	  str = self.gsub "&" , "&amp;"
	  str = str.gsub "<" , "&lt;"
	  str = str.gsub ">" , "&gt;"
	  str = str.gsub "\"" , "&quot;"
	  str = str.gsub "'" , "&#039;"
	end
  end
end
