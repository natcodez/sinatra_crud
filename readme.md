# Sinatra CRUD

One summer a friend (Christian Cito) and I had an idea for a little startup. When we both started university in the fall we didn't have enough time to actually launch it. The code is rough on the edges but I think it's useful for people who are starting to get into programming.

We learned a ton because we wrote almost everything from scratch. The whole project was fun and a great experience.

**Technologies used**: HTML, SCSS, Ruby, Sinatra
