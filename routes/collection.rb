class TravelingDrunkman < Sinatra::Base
  # CREATE
  get '/admin/collections/new' do
    protected!
    erb :'admin/collection/new-collection',
        :layout => :'admin/layout',
        :locals => {:languages => Language.all,
                    :locations => Location.where(:published => true)}
  end

  post '/admin/collections/new' do
    protected!
    collection = Collection.new

    params[:trip_locations].split(",").each do |location_id|
      relation = collection.collection_location_connects.build
      relation.collection_id = collection.id
      relation.location_id = location_id
      relation.save
    end

    collection.title_de = params[:title_de].escape_html_special_characters
    collection.description_de = params[:description_de].escape_html_special_characters
    collection.title_en = params[:title_en].escape_html_special_characters
    collection.description_en = params[:description_en].escape_html_special_characters

    collection.save


    file_name_new = "collection_#{collection.id}.jpeg"

    file_name_new = "collection_#{collection.id}.jpeg"
    File.open("./public/images/#{file_name_new}", 'wb') do |f|
      f.write(open(params[:img_url]).read)
    end

    collection.image_url = file_name_new

    collection.save

    collection.to_json
  end

  post '/admin/collections/togglepublish/:id' do
    collection = Collection.find(params[:id])
    collection.published = !collection.published
    collection.save
    {:success => 1}.to_json
  end

  # READ
  get '/admin/collections/:id' do
    protected!
    collection = Collection.find(params[:id])
    locations = Location.all

    erb :'admin/collection/collection',
        :layout => :'admin/layout',
        :locals => {:collection => collection,
                    :locations => locations}
  end

  get '/admin/collections' do
    protected!
    collections = Collection.all
    erb :'admin/collection/collections', :layout => :'admin/layout', :locals => {:collections => collections}
  end


  # UPDATE
  get '/admin/collections/edit/:id' do
    protected!
    collection = Collection.find(params[:id])
    erb :'admin/collection/edit-collection',
        :layout => :'admin/layout',
        :locals => {:collection => collection,
                    :locations => Location.where(:published => true)
                  }
  end

  post '/admin/collections/edit/:id' do
    protected!
    collection = Collection.find_by(id: params[:id])

    collection.collection_location_connects.destroy_all
    params[:trip_locations].split(",").each do |location_id|
      relation = collection.collection_location_connects.build
      relation.collection_id = collection.id
      relation.location_id = location_id
      relation.save
    end

    collection.title_de = params[:title_de].escape_html_special_characters
    collection.description_de = params[:description_de].escape_html_special_characters
    collection.title_en = params[:title_en].escape_html_special_characters
    collection.description_en = params[:description_en].escape_html_special_characters

    collection.save

    if params[:img_url] != ""
      file_name_new = "collection_#{collection.id}.jpeg"
      File.open("./public/images/#{file_name_new}", 'wb') do |f|
        f.write(open(params[:img_url]).read)
      end
    end

    collection.image_url = file_name_new

    collection.save

    collection.to_json
  end

  #DELETE
  delete '/admin/collections/:id' do
    protected!
    Collection.destroy(params[:id])
    {:success => 1}.to_json
  end
end
