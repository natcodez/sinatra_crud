class TravelingDrunkman < Sinatra::Base

  get '/' do
    trips = Trip.where(:published => true)
	  trip_categories = TripCategory.joins(:trips).distinct
    collections = Collection.where(:published => true)
    erb :'client/index',
        :layout => :'client/layout',
        :locals => {
					  :trips           => trips,
					  :trip_categories => trip_categories,
            :collections => collections
				   }
  end

  get '/about' do
    page = Page.find_by(:name => "about")
    erb :'client/about',
        :layout => :'client/layout',
        :locals => {:about => page}
  end

  get '/contact' do
    page = Page.find_by(:name => "contact")
    erb :'client/contact',
        :layout => :'client/layout',
        :locals => {:contact => page}
  end

  post '/contact' do
    Pony.mail({ :to => "dashintowien@gmail.com",
              :via => :smtp,
              :from => "#{params[:email]}",
              :subject => "Message from #{params[:name]} @ #{Time.now.strftime("%I:%M %d.%m.%m")}",
              :body => "Email: #{params[:email]} \n Message:\n #{params[:message]}",
              :via_options => {
                :address              => 'smtp.gmail.com',
                :port                 => '587',
                :enable_starttls_auto => true,
                :user_name            => 'xxxxx@xxxxxx.com',
                :password             => 'xxxxxxxxx',
                :authentication       => :plain, # :plain, :login, :cram_md5, no auth by default
                :domain => "HELO", # don't know exactly what should be here
              }
    })
    {:success => 1}.to_json
  end

  get '/imprint' do
    page = Page.find_by(:name => "imprint")
    erb :'client/imprint',
        :layout => :'client/layout',
        :locals => {:imprint => page}
  end

  get '/sitemap' do
    page = Page.find_by(:name => "sitemap")
    erb :'client/sitemap',
        :layout => :'client/layout',
        :locals => {:sitemap => page,
                    :pages => Page.all,
                    :trips => Trip.all}
  end

  get '/trips/:id/*' do
    trip = Trip.find(params[:id])
    @id_for_header = "is-single-trip"
    erb :'client/trip',
        :layout => :'client/layout',
        :locals => {:trip => trip}
  end

  get '/collections/:id/*' do
    collection = Collection.find(params[:id])
    @id_for_header = "is-single-trip"
    erb :'client/collection',
        :layout => :'client/layout',
        :locals => {:collection => collection}
  end
end
