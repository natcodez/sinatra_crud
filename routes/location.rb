class TravelingDrunkman < Sinatra::Base
  # CREATE
  get '/admin/locations/new' do
    protected!
    location_categories = LocationCategory.all
    languages = Language.all
    erb :'admin/location/new-location',
        :layout => :'admin/layout',
        :locals => {:location_categories => location_categories,
                    :languages => languages}
  end

  post '/admin/locations/new' do
    protected!
    location = Location.new
    location.street = params[:street].escape_html_special_characters
    location.city = params[:city].escape_html_special_characters
    location.postal_code = params[:postal_code].escape_html_special_characters
    location.country_code = params[:country_code].escape_html_special_characters
    location.latitude = params[:latitude]
    location.longitude = params[:longitude]
    location.minutes_duration = params[:minutes_duration]
    location.location_category = LocationCategory.find(params[:location_category])

    location.title_de = params[:title_de].escape_html_special_characters
    location.description_de = params[:description_de].escape_html_special_characters
    location.title_en = params[:title_en].escape_html_special_characters
    location.description_en = params[:description_en].escape_html_special_characters

    location.save

    file_name_new = "location_#{location.id}.jpeg"
    File.open("./public/images/#{file_name_new}", 'wb') do |f|
      f.write(open(params[:img_url]).read)
    end

    location.image_url = file_name_new

    location.save

    location.to_json
  end

  post '/admin/locations/togglepublish/:id' do
    location = Location.find(params[:id])
    location.published = !location.published
    location.save
    {:success => 1}.to_json
  end

  # READ

  get '/admin/locations/:id' do
    protected!
    location = Location.find_by(id: params[:id])
    erb :'admin/location/location',
        :layout => :'admin/layout',
        :locals => {:location => location}
  end

  get '/admin/locations' do
    protected!
    locations = Location.all
    erb :'admin/location/locations', :layout => :'admin/layout', :locals => {:locations => locations}
  end

  # UPDATE

  get '/admin/locations/edit/:id' do
    protected!
    location = Location.find_by(id: params[:id])
    location_categories = LocationCategory.all

    erb :'admin/location/edit-location',
        :layout => :'admin/layout',
        :locals => {:location => location,
                    :location_categories => location_categories}
  end

  post '/admin/locations/edit/:id' do
    protected!
    location = Location.find_by(:id => params[:id])
    location.street = params[:street].escape_html_special_characters
    location.city = params[:city].escape_html_special_characters
    location.postal_code = params[:postal_code]
    location.country_code = params[:country_code]
    location.latitude = params[:latitude]
    location.longitude = params[:longitude]
    location.minutes_duration = params[:minutes_duration]
    location.location_category = LocationCategory.find(params[:location_category])

    location.title_de = params[:title_de].escape_html_special_characters
    location.description_de = params[:description_de].escape_html_special_characters
    location.title_en = params[:title_en].escape_html_special_characters
    location.description_en = params[:description_en].escape_html_special_characters

    location.save

    if params[:img_url] != ""
      file_name_new = "location_#{location.id}.jpeg"
      File.open("./public/images/#{file_name_new}", 'wb') do |f|
        f.write(open(params[:img_url]).read)
      end
      location.save
    end

    location.to_json
    end

  # DELETE

  delete '/admin/locations/:id' do
    protected!
    location = Location.find(params[:id])
    if location.trips.count == 0
      Location.destroy(params[:id])
      {
        :success => 1,
        :error_msg => ""
      }.to_json
    else
      {
        :success => 0,
        :error_msg => "Error: Can't delete because '#{location.translation(@lang).title}' is included in Trips"
      }.to_json
    end
  end
end
