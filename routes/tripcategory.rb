class TravelingDrunkman < Sinatra::Base
  # CREATE

  get '/admin/tripcategories/new' do
    protected!
    languages = Language.all
    erb :'admin/tripcategory/new-tripcategory',
        :layout => :'admin/layout',
        :locals => {:languages => languages}
  end

  post '/admin/tripcategories/new' do
    protected!

    tripcategory = TripCategory.new
    tripcategory.color_hex = params[:colorhex]

    tripcategory.title_de = params[:title_de].escape_html_special_characters
    tripcategory.description_de = params[:description_de].escape_html_special_characters
    tripcategory.title_en = params[:title_en].escape_html_special_characters
    tripcategory.description_en = params[:description_en].escape_html_special_characters

    tripcategory.save

    tripcategory.to_json
  end

  post '/admin/tripcategories/togglepublish/:id' do
    tripcategory = TripCategory.find(params[:id])
    tripcategory.published = !tripcategory.published
    tripcategory.save
    {:success => 1}.to_json
  end

  # READ

  get '/admin/tripcategories/:id' do
    protected!
    tripcategory = TripCategory.find_by(id: params[:id])
    erb :'admin/tripcategory/tripcategory', :layout => :'admin/layout', :locals => {:tripcategory => tripcategory}
  end

  get '/admin/tripcategories' do
    protected!
    tripcategories = TripCategory.all
    erb :'admin/tripcategory/tripcategories', :layout => :'admin/layout', :locals => {:tripcategories => tripcategories}
  end

  # UPDATE

  get '/admin/tripcategories/edit/:id' do
    protected!
    tripcategory = TripCategory.find_by(id: params[:id])
    erb :'admin/tripcategory/edit-tripcategory', :layout => :'admin/layout', :locals => {:tripcategory => tripcategory}
  end

  post '/admin/tripcategories/edit/:id' do
    protected!
    tripcategory = TripCategory.find_by(id: params[:id])

    tripcategory.color_hex = params[:colorhex]

    tripcategory.title_de = params[:title_de].escape_html_special_characters
    tripcategory.description_de = params[:description_de].escape_html_special_characters
    tripcategory.title_en = params[:title_en].escape_html_special_characters
    tripcategory.description_en = params[:description_en].escape_html_special_characters

    tripcategory.save
    tripcategory.to_json
  end

  # DELETE

  delete '/admin/tripcategories/:id' do
    protected!
    tripcategory = TripCategory.find(params[:id])
    if tripcategory.trips.count == 0
      TripCategory.destroy(params[:id])
      {
        :success => 1,
        :error_msg => ""
      }.to_json
    else
      {
        :success => 0,
        :error_msg => "Error: Can't delete because '#{tripcategory.translation(@lang).title}' has active Trips"
      }.to_json
    end
  end
end
