class TravelingDrunkman < Sinatra::Base
  get '/admin' do
    protected!
    new_trips = Trip.last(5)
    new_locations = Location.last(5)
    new_pages = Page.last(5)
    new_tripcategories = TripCategory.last(5)
    new_locationcategories = LocationCategory.last(5)
    new_collections = Collection.last(5)
    erb :'admin/index',
        :layout => :'admin/layout',
        :locals => {:trips => new_trips,
                    :locations => new_locations,
                    :pages => new_pages,
                    :tripcategories => new_tripcategories,
                    :locationcategories => new_locationcategories,
                    :collections => new_collections}
  end
end
