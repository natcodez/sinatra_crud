class TravelingDrunkman < Sinatra::Base
  get '/admin/import' do
    protected!
    erb :'admin/import_export/import',
        :layout => :'admin/layout',
        :locals => {:models => ActiveRecord::Base.descendants}
  end

  # Remove in production
  get '/admin/import/delete_all' do
    protected!
    ActiveRecord::Base.descendants.each do |model|
      model.destroy_all
    end
    Language.create(:name => "German", :language_code => "de")
    Language.create(:name => "English", :language_code => "en")
    redirect to('/admin/import')
  end

  post '/admin/import/all' do
    protected!
    json = JSON.parse(params[:all_json][:tempfile].read)
    ActiveRecord::Base.descendants.each do |model|
      jsonToModel(json, model)
    end
    {:success => 1}.to_json
  end

  post '/admin/import/:model' do
    protected!
    json = JSON.parse(params[:model_json][:tempfile].read)
    jsonToModel(json, params[:model].capitalize.constantize)
    {:success => 1}.to_json
  end

  get '/admin/export' do
    protected!
    erb :'admin/import_export/export',
        :layout => :'admin/layout',
        :locals => {:models => ActiveRecord::Base.descendants}
  end

  get '/admin/export/all.json' do
    protected!
    content_type :json
    json = "{"
    ActiveRecord::Base.descendants.each do |model|
      json << modelToJSON(model)
      json << ","
    end
    json.chomp!(",") # Remove trailing comma
    json << "}"
    json
  end

  get '/admin/export/:model.json' do
    protected!
    content_type :json
    modelToJSON(params[:model].capitalize.constantize)
  end
end
