class TravelingDrunkman < Sinatra::Base
  get '/admin/locationcategories/new' do
    protected!
    languages = Language.all
    erb :'admin/locationcategory/new-locationcategory',
        :layout => :'admin/layout',
        :locals => {:languages => languages}
  end

  post '/admin/locationcategories/new' do
    protected!
    locationcategory = LocationCategory.new
    locationcategory.color_hex = params[:colorhex]

    locationcategory.title_de = params[:title_de].escape_html_special_characters
    locationcategory.description_de = params[:description_de].escape_html_special_characters
    locationcategory.title_en = params[:title_en].escape_html_special_characters
    locationcategory.description_en = params[:description_en].escape_html_special_characters

    locationcategory.save

    locationcategory.to_json
  end

  post '/admin/locationcategories/togglepublish/:id' do
    locationcategory = LocationCategory.find(params[:id])
    locationcategory.published = !locationcategory.published
    locationcategory.save
    {:success => 1}.to_json
  end

  # READ

  get '/admin/locationcategories/:id' do
    protected!
    locationcategory = LocationCategory.find_by(id: params[:id])
    erb :'admin/locationcategory/locationcategory', :layout => :'admin/layout', :locals => {:locationcategory => locationcategory}
  end

  get '/admin/locationcategories' do
    protected!
    locationcategories = LocationCategory.all
    erb :'admin/locationcategory/locationcategories', :layout => :'admin/layout', :locals => {:locationcategories => locationcategories}
  end

  # UPDATE

  get '/admin/locationcategories/edit/:id' do
    protected!
    locationcategory = LocationCategory.find_by(id: params[:id])
    languages = Language.all
    erb :'admin/locationcategory/edit-locationcategory',
        :layout => :'admin/layout',
        :locals => {:locationcategory => locationcategory,
                    :languages => languages}
  end

  post '/admin/locationcategories/edit/:id' do
    protected!
    locationcategory = LocationCategory.find_by(:id => params[:id])
    locationcategory.color_hex = params[:colorhex]

    locationcategory.title_de = params[:title_de].escape_html_special_characters
    locationcategory.description_de = params[:description_de].escape_html_special_characters
    locationcategory.title_en = params[:title_en].escape_html_special_characters
    locationcategory.description_en = params[:description_en].escape_html_special_characters

    locationcategory.save

    locationcategory.to_json
  end

  # DELETE

  delete '/admin/locationcategories/:id' do
    protected!
    locationcategory = LocationCategory.find(params[:id])
    if locationcategory.locations.count == 0
      LocationCategory.destroy(params[:id])
      {
        :success => 1,
        :error_msg => ""
      }.to_json
    else
      {
        :success => 0,
        :error_msg => "Error: Can't delete because '#{locationcategory.translation(@lang).title}' has active Locations"
      }.to_json
    end
  end
end
