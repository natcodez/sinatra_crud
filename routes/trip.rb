class TravelingDrunkman < Sinatra::Base
  # CREATE
  get '/admin/trips/new' do
    protected!
    erb :'admin/trip/new-trip',
        :layout => :'admin/layout',
        :locals => {:languages => Language.all,
                    :trip_categories => TripCategory.all,
                    :locations => Location.where(:published => true)}
  end

  post '/admin/trips/new' do
    protected!
    trip = Trip.new

    trip.minutes_duration = params[:minutes_duration]
    trip.trip_category_id = params[:trip_category]
    params[:trip_locations].split(",").each_with_index do |location_id, index|
      relation = trip.trip_location_connects.build
      relation.trip_id = trip.id
      relation.location_id = location_id
      relation.trip_position = index
      relation.save
    end

    trip.title_de = params[:title_de].escape_html_special_characters
    trip.description_de = params[:description_de].escape_html_special_characters
    trip.title_en = params[:title_en].escape_html_special_characters
    trip.description_en = params[:description_en].escape_html_special_characters

    trip.save

    file_name_new = "trip_#{trip.id}.jpeg"

    File.open("./public/images/#{file_name_new}", 'wb') do |f|
      f.write(open(params[:img_url]).read)
    end

    trip.image_url = file_name_new

    trip.save

    trip.to_json
  end

  post '/admin/trips/togglepublish/:id' do
    trip = Trip.find(params[:id])
    trip.published = !trip.published
    trip.save
    {:success => 1}.to_json
  end

  # READ
  get '/admin/trips/:id' do
    protected!
    trip = Trip.find_by(id: params[:id])
    locations = Location.all

    erb :'admin/trip/trip',
        :layout => :'admin/layout',
        :locals => {:trip => trip,
                    :locations => locations}
  end

  get '/admin/trips' do
    protected!
    trips = Trip.all
    erb :'admin/trip/trips', :layout => :'admin/layout', :locals => {:trips => trips}
  end


  # UPDATE
  get '/admin/trips/edit/:id' do
    protected!
    trip = Trip.find_by(id: params[:id])
    erb :'admin/trip/edit-trip',
        :layout => :'admin/layout',
        :locals => {:trip => trip,
                    :trip_categories => TripCategory.all,
                    :locations => Location.where(:published => true)
                  }
  end

  post '/admin/trips/edit/:id' do
    protected!
    trip = Trip.find_by(id: params[:id])

    trip.minutes_duration = params[:minutes_duration]
    trip.trip_category_id = params[:trip_category]

    trip.trip_location_connects.destroy_all
    params[:trip_locations].split(",").each_with_index do |location_id, index|
      relation = trip.trip_location_connects.build
      relation.trip_id = trip.id
      relation.location_id = location_id
      relation.trip_position = index
      relation.save
    end

    trip.title_de = params[:title_de].escape_html_special_characters
    trip.description_de = params[:description_de].escape_html_special_characters
    trip.title_en = params[:title_en].escape_html_special_characters
    trip.description_en = params[:description_en].escape_html_special_characters

    trip.save

    if params[:img_url] != ""
      file_name_new = "trip_#{trip.id}.jpeg"
      File.open("./public/images/#{file_name_new}", 'wb') do |f|
        f.write(open(params[:img_url]).read)
      end
    end

    trip.image_url = file_name_new

    trip.save
    trip.to_json
  end

  #DELETE
  delete '/admin/trips/:id' do
    protected!
    Trip.destroy(params[:id])
    {:success => 1}.to_json
  end
end
