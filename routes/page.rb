class TravelingDrunkman < Sinatra::Base
  # CREATE

  get '/admin/pages/new' do
    protected!
    erb :'admin/page/new-page', :layout => :'admin/layout'
  end

  post '/admin/pages/new' do
    protected!
    page = Page.new
    page.name = params[:name]

    page.title_de = params[:title_de].escape_html_special_characters
    page.description_de = params[:description_de].escape_html_special_characters
    page.title_en = params[:title_en].escape_html_special_characters
    page.description_en = params[:description_en].escape_html_special_characters

    page.save

    page.to_json
  end

  post '/admin/pages/togglepublish/:id' do
    page = Page.find(params[:id])
    page.published = !page.published
    page.save
    {:success => 1}.to_json
  end

  # READ

  get '/admin/pages/:id' do
    protected!
    page = Page.find_by(id: params[:id])
    erb :'admin/page/page', :layout => :'admin/layout', :locals => {:page => page}
  end

  get '/admin/pages' do
    protected!
    pages = Page.all
    erb :'admin/page/pages', :layout => :'admin/layout', :locals => {:pages => pages}
  end

  # UPDATE

  get '/admin/pages/edit/:id' do
    protected!
    page = Page.find_by(id: params[:id])
    erb :'admin/page/edit-page', :layout => :'admin/layout', :locals => {:page => page}
  end

  post '/admin/pages/edit/:id' do
    protected!
    page = Page.find_by(id: params[:id])
    page.name = params[:name]

    page.title_de = params[:title_de].escape_html_special_characters
    page.description_de = params[:description_de].escape_html_special_characters
    page.title_en = params[:title_en].escape_html_special_characters
    page.description_en = params[:description_en].escape_html_special_characters

    page.save
    page.to_json
  end

  # DELETE

  delete '/admin/pages/:id' do
    protected!
    Page.destroy(params[:id])
    {:success => 1}.to_json
  end
end
