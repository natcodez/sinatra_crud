# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170930103051) do

  create_table "collection_location_connects", force: :cascade do |t|
    t.integer "collection_id"
    t.integer "location_id"
  end

  create_table "collections", force: :cascade do |t|
    t.string "title", default: ""
    t.string "description", default: ""
    t.string "title_de", default: ""
    t.string "description_de", default: ""
    t.string "title_en", default: ""
    t.string "description_en", default: ""
    t.boolean "published", default: false
    t.string "image_url"
  end

  create_table "languages", force: :cascade do |t|
    t.string "name"
    t.string "language_code"
  end

  create_table "location_categories", force: :cascade do |t|
    t.string "color_hex"
    t.boolean "published", default: false
    t.string "title", default: ""
    t.string "description", default: ""
    t.string "title_de", default: ""
    t.string "description_de", default: ""
    t.string "title_en", default: ""
    t.string "description_en", default: ""
  end

  create_table "locations", force: :cascade do |t|
    t.string "street"
    t.string "city"
    t.string "postal_code"
    t.string "country_code"
    t.string "latitude"
    t.string "longitude"
    t.integer "minutes_duration"
    t.integer "location_category_id"
    t.string "image_url"
    t.boolean "published", default: false
    t.string "title", default: ""
    t.string "description", default: ""
    t.string "title_de", default: ""
    t.string "description_de", default: ""
    t.string "title_en", default: ""
    t.string "description_en", default: ""
  end

  create_table "pages", force: :cascade do |t|
    t.string "name"
    t.boolean "published", default: false
    t.string "title", default: ""
    t.string "description", default: ""
    t.string "title_de", default: ""
    t.string "description_de", default: ""
    t.string "title_en", default: ""
    t.string "description_en", default: ""
  end

  create_table "trip_categories", force: :cascade do |t|
    t.string "color_hex"
    t.boolean "published", default: false
    t.string "title", default: ""
    t.string "description", default: ""
    t.string "title_de", default: ""
    t.string "description_de", default: ""
    t.string "title_en", default: ""
    t.string "description_en", default: ""
  end

  create_table "trip_location_connects", force: :cascade do |t|
    t.integer "trip_id"
    t.integer "location_id"
    t.integer "trip_position"
  end

  create_table "trips", force: :cascade do |t|
    t.integer "trip_category_id"
    t.string "minutes_duration"
    t.string "image_url"
    t.boolean "published", default: false
    t.string "title", default: ""
    t.string "description", default: ""
    t.string "title_de", default: ""
    t.string "description_de", default: ""
    t.string "title_en", default: ""
    t.string "description_en", default: ""
  end

end
