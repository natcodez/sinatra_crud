class CreateCollection < ActiveRecord::Migration[5.1]
  def change
    create_table "collections" , force: :cascade do |t|
      t.string :title, :default => ""
      t.string :description, :default => ""
      t.string :title_de, :default => ""
      t.string :description_de, :default => ""
      t.string :title_en, :default => ""
      t.string :description_en, :default => ""
      t.boolean :published, :default => 0
      t.string :image_url
    end

    create_table "collection_location_connects" , force: :cascade do |t|
      t.integer :collection_id
      t.integer :location_id
    end
  end
end
