class ChangeTranslationStructure < ActiveRecord::Migration[5.1]
  def change
    drop_table :trip_translations
    drop_table :trip_category_translations
    drop_table :location_translations
    drop_table :location_category_translations
    drop_table :page_translations

    tables = [:trips, :trip_categories,
              :locations, :location_categories,
              :pages]
    tables.each do |table_name|
      add_column table_name, :title, :string, :default => ""
      add_column table_name, :description, :string, :default => ""
      add_column table_name, :title_de, :string, :default => ""
      add_column table_name, :description_de, :string, :default => ""
      add_column table_name, :title_en, :string, :default => ""
      add_column table_name, :description_en, :string, :default => ""
    end
  end
end
