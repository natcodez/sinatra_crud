class CreatePublicPrivateSwitch < ActiveRecord::Migration[5.1]
  def change
    tables = [:trips, :trip_categories,
              :locations, :location_categories,
              :pages]
    tables.each do |table_name|
      add_column table_name, :published, :boolean, :default => 0
    end
  end
end
