class FirstMigration < ActiveRecord::Migration[5.1]
  def change
  	create_table "trips" , force: :cascade do |t|
        t.integer :trip_category_id
        t.string :minutes_duration
      end

  	create_table "trip_translations" , force: :cascade do |t|
  	  t.integer :trip_id
  	  t.integer :language_id
  	  t.string :title
  	  t.text :description
  	end

    create_table "trip_categories" , force: :cascade do |t|
      t.string :color_hex
    end

  	create_table "trip_category_translations" , force: :cascade do |t|
  	  t.integer :trip_category_id
  	  t.integer :language_id
  	  t.string :title
  	  t.text :description
  	end

  	create_table "trip_location_connects" , force: :cascade do |t|
      t.integer :trip_id
      t.integer :location_id
      t.integer :trip_position
    end


      #Locations and Locations translation Table
  	create_table "locations" , force: :cascade do |t|
  	  t.string :street
  	  t.string :city
  	  t.string :postal_code
  	  t.string :country_code
  	  t.string :latitude
  	  t.string :longitude
  	  t.integer :minutes_duration
  	  t.integer :location_category_id
    end

  	create_table "location_translations" , force: :cascade do |t|
  	  t.integer :location_id
  	  t.integer :language_id
  	  t.string :title
  	  t.text :description
  	end

    create_table "location_categories" , force: :cascade do |t|
      t.string :color_hex
    end

  	create_table "location_category_translations" , force: :cascade do |t|
  	  t.integer :location_category_id
  	  t.integer :language_id
  	  t.string :title
  	  t.text :description
  	end


    create_table "pages" , force: :cascade do |t|
  	  t.string :name
    end

  	create_table "page_translations" , force: :cascade do |t|
  	  t.integer :page_id
  	  t.integer :language_id
  	  t.string :title
  	  t.text :description
  	end

  	create_table "languages" , force: :cascade do |t|
  	  t.string :name
  	  t.string :language_code
  	end
  end
end
