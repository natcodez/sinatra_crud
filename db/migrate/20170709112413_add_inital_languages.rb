class AddInitalLanguages < ActiveRecord::Migration[5.1]
  def change
    Language.create(:name => "German", :language_code => "de")
    Language.create(:name => "English", :language_code => "en")
  end
end
