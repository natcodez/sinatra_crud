class AddedImages < ActiveRecord::Migration[5.1]
  def change
    change_table :trips do |t|
  	  t.string :image_url
    end

  	change_table :locations do |t|
  	  t.string :image_url
    end
  end
end
