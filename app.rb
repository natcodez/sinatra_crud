require 'sinatra'
require 'sinatra/activerecord'
require 'json'
require 'pony'
require 'csv'
require "open-uri"

require_relative "helpers/helpers"
require_relative 'routes/init'

require_relative "models"
require_relative "localization/translation"
require_relative "localization/localization"

class TravelingDrunkman < Sinatra::Base
  set :database, "sqlite3:app.sqlite3"

  enable :sessions

  configure do
    set :app_file, __FILE__
  end

  helpers Helpers

  before do
    if params[:lang] and Language.find_by(:language_code => params[:lang])
      session[:lang] = params[:lang]
    end

    lang_code = session[:lang]
    if lang_code and Language.find_by(:language_code => lang_code)
      @lang = Language.find_by(:language_code => lang_code)
    else
      @lang = Language.find_by(:language_code => "en")
    end

    @labels = LabelTranslation.labels(@lang.language_code)
    @id_for_header = ""
  end

  after do
    ActiveRecord::Base.clear_active_connections!
  end
end
