#Trip tables
class Trip < ActiveRecord::Base
  has_many :trip_location_connects , :dependent => :destroy
  has_many :locations, through: :trip_location_connects
  belongs_to :trip_category
end

class TripCategory < ActiveRecord::Base
  has_many :trips
end

class TripLocationConnect < ActiveRecord::Base
  belongs_to :trip
  belongs_to :location
end

#Locaction tables
class Location < ActiveRecord::Base
  has_many :trip_location_connects , :dependent => :destroy
  has_many :trips, through: :trip_location_connects
  belongs_to :location_category
end


class LocationCategory < ActiveRecord::Base
  has_many :locations
end


# Collection tables

class Collection < ActiveRecord::Base
  has_many :collection_location_connects , :dependent => :destroy
  has_many :locations, through: :collection_location_connects
end

class CollectionLocationConnect < ActiveRecord::Base
  belongs_to :collection
  belongs_to :location
end

#Page tables
class Page < ActiveRecord::Base
end

#Language table
class Language < ActiveRecord::Base
end
